FROM debian:buster-slim as builder

RUN apt-get -qy update && apt-get -qy upgrade && apt-get -qy install software-properties-common wget gnupg

RUN wget -q -O - https://packages.grafana.com/gpg.key | apt-key add -

RUN echo "deb https://packages.grafana.com/enterprise/deb stable main" | tee -a /etc/apt/sources.list.d/grafana.list

#RUN apt-get update && apt-get -qy install grafana-enterprise

#WORKDIR grafana

RUN wget https://dl.grafana.com/oss/release/grafana-7.5.1.linux-amd64.tar.gz
RUN tar -zxvf grafana-7.5.1.linux-amd64.tar.gz

RUN mv grafana-7.5.1 grafana

WORKDIR grafana
CMD ./bin/grafana-server web

#CMD grafana-server web --homepath /var/lib/grafana --config /etc/grafana
